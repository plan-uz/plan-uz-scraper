import models.Room;
import scrapers.buildings.BuildingsScraper;
import scrapers.rooms.RoomsScraper;

import java.util.ArrayList;

public class RoomScraperDemo {
    public static void main(String[] args) {
        BuildingsScraper buildingsScraper = new BuildingsScraper();
        System.out.println(buildingsScraper.getBuildings().get(0).getBuildingName());
        RoomsScraper roomsScraper = new RoomsScraper(buildingsScraper.getBuildings().get(0).getRoomsListLink());
        ArrayList<Room> rooms = roomsScraper.getRooms();
        System.out.println("-------");
        System.out.println(rooms.get(0).getRoomName());
        System.out.println(rooms.get(0).getSeatsNumber());
        System.out.println(rooms.get(0).getRoomType());
        System.out.println(rooms.get(0).getMaintainer());
        System.out.println(rooms.get(0).getScheduleID());
    }
}
