package scrapers.groupsSchedule;

import models.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import utils.Links;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GroupsScheduleScraper {
    private final String groupScheduleUrlPrefix = "http://www.plan.uz.zgora.pl/grupy_plan.php?pId_Obiekt=";
    private Document doc;
    private Group group;

    public GroupsScheduleScraper() {}

    public GroupsScheduleScraper(Group group) {
        this.group = group;
    }

    public ArrayList<GroupSchedule> getGroupSchedule() {
        try {
            Map<String, List<String>> parameters = Links.splitQuery(group.getScheduleLink().toURL());
            int scheduleID = Integer.parseInt(parameters.get("pId_Obiekt").get(0));
            return getGroupSchedule(scheduleID);
        } catch (MalformedURLException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<GroupSchedule> getGroupSchedule(int scheduleID) {
        try {
            doc = Jsoup.connect(groupScheduleUrlPrefix+scheduleID).get();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Elements elements = doc.select("table tr td");
        ArrayList<GroupSchedule> groupSchedules = new ArrayList<>();

        for (int counter = 0; counter < elements.size(); counter++) {
            if (TermsTable.isCalendarDay(elements.get(counter).text())) {
                if (counter == elements.size()-1) break;
                for (int i=counter+1; TermsTable.isCalendarDay(elements.get(i).text()) == false; i++) {
                    if (i == elements.size() - 1) break;
                    if ((elements.get(i).text().length() == 1) || (elements.get(i).text().length() == 0)) {
                        if (elements.get(i).text().length() == 0 || GroupSchedule.isSubgroup(elements.get(i).text().charAt(0))) {
                            GroupSchedule groupSchedule = new GroupSchedule();
                            String[] time;
                            groupSchedule.setCalendarDay(elements.get(counter).text());
                            if (elements.get(i).text().length() == 1) groupSchedule.setSubgroup(elements.get(i).text().charAt(0));
                            time = elements.get(i+1).text().split(":");
                            groupSchedule.setStartTime(new Time(Integer.parseInt(time[0]), Integer.parseInt(time[1])));
                            time = elements.get(i+2).text().split(":");
                            groupSchedule.setEndTime(new Time(Integer.parseInt(time[0]), Integer.parseInt(time[1])));
                            groupSchedule.setSubjectName(elements.get(i+3).text());
                            groupSchedule.setSubjectType(elements.get(i+4).text().charAt(0));
                            try {
                                groupSchedule.setTeacher(new Teacher(elements.get(i+5).text(), new URI(elements.get(i+5).selectFirst("a").attr("href"))));
                                groupSchedule.setRoom(new Room(elements.get(i+6).text(), new URI(elements.get(i+6).selectFirst("a").attr("href"))));
                                groupSchedule.setTerm(new Term(elements.get(i+7).text(), new URI(elements.get(i+7).selectFirst("a").attr("href"))));
                            } catch (URISyntaxException e) {
                                e.printStackTrace();
                            }
                            groupSchedules.add(groupSchedule);
                        }
                    }
                }
            }
        }

        return groupSchedules;
    }
}
