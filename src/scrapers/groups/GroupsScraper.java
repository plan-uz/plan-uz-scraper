package scrapers.groups;

import models.Term;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class GroupsScraper {
    private final String groupUrlPrefix = "http://www.plan.uz.zgora.pl/";
    private Document doc;

    public ArrayList<Term> getGroups(URI courseUri) {
        try {
            doc = Jsoup.connect(courseUri.toString()).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Elements elements = doc.select("table tr td");
        ArrayList<Term> groups = new ArrayList();
        for (int counter = 0; counter < elements.size(); counter++) {
            Term term = new Term();
            term.setTermName(elements.get(counter).text().replaceAll("\\s\\(.+$", ""));
            try {
                term.setTermLink(new URI(
                        groupUrlPrefix + elements.get(counter).select("a").attr("href"))
                );
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            groups.add(term);
        }
        return groups;
    }
}
