package scrapers.courses;

import models.Term;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class CoursesScraper {
    private final String allCoursesUrl = "http://www.plan.uz.zgora.pl/grupy_lista_kierunkow.php";
    private final String courseUrlPrefix = "http://www.plan.uz.zgora.pl/";
    private Document doc;

    public CoursesScraper() {
        {
            try {
                doc = Jsoup.connect(allCoursesUrl).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Term> getCourses() {
        Elements elements = doc.select(".container.main > .list-group > .list-group-item");
        elements = elements.get(5).select(".list-group-item > .list-group > .list-group-item");
        ArrayList<Term> courses = new ArrayList();
        for (int counter = 0; counter < elements.size(); counter++) {
            Term term = new Term();
            term.setTermName(elements.get(counter).text().replaceAll("\\s\\(.+$", ""));
            try {
                term.setTermLink(new URI(
                        courseUrlPrefix + elements.get(counter).select("a").attr("href"))
                );
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            courses.add(term);
        }
        return courses;
    }
}
