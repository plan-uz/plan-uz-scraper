package scrapers.rooms;

import models.Room;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RoomsScraper {
    private final String roomsUrlPrefix = "http://www.plan.uz.zgora.pl/";

    private Document doc;

    public RoomsScraper(URI buildingLink) {
        {
            try {
                doc = Jsoup.connect(buildingLink.toString()).get();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Room> getRooms() {
        Elements elements = doc.select("tr:not(.gray)");
        ArrayList<Room> rooms = new ArrayList();
        for (int counter = 0; counter < elements.size(); counter++) {
            Room room = new Room();
            room.setRoomName(elements.get(counter).select("td a").first().text());
            room.setSeatsNumber(elements.get(counter).select("td").get(1).text());
            room.setRoomType(elements.get(counter).select("td").get(2).text());
            room.setMaintainer(elements.get(counter).select("td").get(3).text());
            try {
                Pattern idPattern = Pattern.compile("(=\\d+)");
                Matcher m = idPattern.matcher(new URI(
                        roomsUrlPrefix + elements.get(counter).select("td a").first().attr("href")
                ).getQuery());
                while (m.find()) {
                    room.setScheduleID(Integer.parseInt(m.group(1).replace("=", "")));
                }
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            rooms.add(room);
        }
        return rooms;
    }
}
