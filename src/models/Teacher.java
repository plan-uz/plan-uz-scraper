package models;

import java.net.URI;

public class Teacher {
    private String teacherName;
    private String unitName;
    private URI scheduleLink;

    public Teacher() {

    }

    public Teacher(String teacherName, URI scheduleLink) {
        this.teacherName = teacherName;
        this.scheduleLink = scheduleLink;
    }

    public String toString() {
        return teacherName;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public URI getScheduleLink() {
        return scheduleLink;
    }

    public void setScheduleLink(URI scheduleLink) {
        this.scheduleLink = scheduleLink;
    }
}
