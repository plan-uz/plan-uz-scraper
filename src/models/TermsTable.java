package models;

import java.util.Date;

public class TermsTable {

    private Term term;
    private int number;
    private Date date;
    private String planDay, calendarDay;

    public TermsTable() {}

    public TermsTable(Term term, int number, Date date, String planDay, String calendarDay) {
        this.term = term;
        this.number = number;
        this.date = date;
        this.planDay = planDay;
        this.calendarDay = calendarDay;
    }

    public static boolean isCalendarDay(String calendarDay) {
        if (calendarDay.equals("Poniedziałek")) return true;
        if (calendarDay.equals("Wtorek")) return true;
        if (calendarDay.equals("Środa")) return true;
        if (calendarDay.equals("Czwartek")) return true;
        if (calendarDay.equals("Piątek")) return true;
        if (calendarDay.equals("Sobota")) return true;
        if (calendarDay.equals("Niedziela")) return true;
        if (calendarDay.equals("Nieregularne")) return true;
        return false;
    }

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPlanDay() {
        return planDay;
    }

    public void setPlanDay(String planDay) {
        this.planDay = planDay;
    }

    public String getCalendarDay() {
        return calendarDay;
    }

    public void setCalendarDay(String calendarDay) {
        this.calendarDay = calendarDay;
    }
}
