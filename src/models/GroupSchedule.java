package models;

public class GroupSchedule {

    private char subgroup, subjectType;
    private Time startTime, endTime;
    private String subjectName;
    private Teacher teacher;
    private Term term;
    private String calendarDay;
    private Room room;

    public GroupSchedule() {
    }

    public GroupSchedule(char subgroup, char subjectType, Time startTime, Time endTime, String subjectName, Teacher teacher, Term term, String calendarDay, Room room) {
        this.subgroup = subgroup;
        this.subjectType = subjectType;
        this.startTime = startTime;
        this.endTime = endTime;
        this.subjectName = subjectName;
        this.teacher = teacher;
        this.term = term;
        this.calendarDay = calendarDay;
        this.room = room;
    }

    public static boolean isSubgroup(char subgroup) {
        switch(subgroup) {
            case 'A':
                return true;
            case 'B':
                return true;
            default:
                return false;
        }
    }

    public char getSubgroup() {
        return subgroup;
    }

    public void setSubgroup(char subgroup) {
        this.subgroup = subgroup;
    }

    public char getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(char subjectType) {
        this.subjectType = subjectType;
    }

    public Time getStartTime() {
        return startTime;
    }

    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }

    public Time getEndTime() {
        return endTime;
    }

    public void setEndTime(Time endTime) {
        this.endTime = endTime;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Term getTerm() {
        return term;
    }

    public void setTerm(Term term) {
        this.term = term;
    }

    public String getCalendarDay() {
        return calendarDay;
    }

    public void setCalendarDay(String calendarDay) {
        this.calendarDay = calendarDay;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }
}
