package models;

import java.net.URI;

public class Room {
    private String roomName;
    private String seatsNumber;
    private String roomType;
    private String maintainer;
    private int scheduleID;
    private URI scheduleLink;

    public int getScheduleID() {
        return scheduleID;
    }

    public void setScheduleID(int scheduleID) {
        this.scheduleID = scheduleID;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getSeatsNumber() {
        return seatsNumber;
    }

    public void setSeatsNumber(String seatsNumber) {
        this.seatsNumber = seatsNumber;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String toString() {
        return roomName;
    }

    public Room() {
    }

    public Room(String roomName, URI scheduleLink) {
        this.roomName = roomName;
        this.scheduleLink = scheduleLink;
    }

    public String getMaintainer() {
        return maintainer;
    }

    public void setMaintainer(String maintainer) {
        this.maintainer = maintainer;
    }

    public URI getScheduleLink() {
        return scheduleLink;
    }

    public void setScheduleLink(URI scheduleLink) {
        this.scheduleLink = scheduleLink;
    }
}
